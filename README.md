### Desafio Site Blindado

## Geral

---

Para iniciar o APP digite 'npm start' no diretório raíz do projeto. O APP estará acessível através do endereço 'http://localhost:3000'.
Para iniciar a API digite 'python3 apy.pi' no diretório API da raíz do projeto. A API estará acessível através do endereço 'http://localhost:8000/api/json/'.

---

# APP

A base do projeto foi criada a través do package Create-React-App.
No topo da aplicação escolhi deixar um jumbotron com o título do desafio como um placeholder para logo/arte/banner. Decidi não adicionar breadcrumbs logo abaixo da topbar porque como não há outra página a ser desenvolvida, seria um componente sem utilidade.
Desenvolvi algo simples, porém funcional, com o intuito de ser facilmente modificável de acordo com os elementos que se queira adicionar/remover da página. Não escolhi cores ou fundos porque acredito que é melhor desenvolver o que foi pedido para depois incrementar do que desenvolver a mais e ter que remover.
Dentro da página utilizo duas API's : a do servidor para guardar as informações inseridas e uma externa para buscar o endereço a partir do CEP.

# API

A API foi desenvolvida em python 3 através da micro-framework Flask.
A API desenvolvida foi simples. Há no total três endpoints: addRegister, para adicionar um registro; GetRegister/CPF, para obter um registro a partir de um CPF e getAllInfo, que retorna toda a informação contida.
Adicionei lógica para evitar a inserção repetida de URLS cadastradas anteriormente (vinculadas a um mesmo CPF). Caso uma lista de 4 URLS seja enviada ao servidor e apenas uma seja nova, apenas esta será inserida.

# SQLITE

A base de dados utilizada contém apenas duas tabelas: 'users' e 'urls'. A chave primária da tabela 'users' é o CPF do utilizador, referenciada pelo campo 'user_cpf' da tabela 'urls'. Inicialmente a ideia era atomizar ainda mais as tabelas, mas como os campos obrigatórios eram apenas nome e CPF, não faria sentido uma tabela com os campos a nulo.

# Testes

Os testes foram direcionados apenas à funções contidas no arquivo 'utils.js' na pasta src/scripts. Como é minha primeira interação com React, o desenvolvimento de testes
para os componentes ainda não está claro para mim. Tive dificuldades em carregar os arquivos .jsx para poder testar os componentes e métodos existentes. Sendo assim, foquei nos métodos utilitários compartilhados entre os componentes.
Para realizar os testes basta digitar 'npm run test' na pasta raíz do projeto. Ao todo são 40 testes.

## Bibliotecas Utilizadas

#APP
React, Bootstrap e Axios.
#API
Flask, Flask-restful, Flask-Cors e sqlite3.
#Testes
Mocha e Chai.
