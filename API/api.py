from flask import Flask
from flask_restful import Resource, Api
from flask_cors import CORS
from flask import request
from src import dbConnection

app = Flask(__name__)
cors = CORS(app, resources={r'*':{"origins": "*"}})
api =  Api(app)

class AddRegister(Resource):
  def post(self):
    REQUIRED_FIELDS = ["personal_name", "personal_cpf", "urls"]
    parameters = request.get_json(force=True)

    for field in REQUIRED_FIELDS:
      if field not in parameters:
        return {"OK" : False, "Message": "O campo " + field + " é obrigatório!"}  
    
    try:
      conn = dbConnection.DB()
      message = "Dados cadastrados com sucesso!"
      isValid = conn.addNewUser(
        parameters['personal_cpf'], parameters['personal_name'], parameters['personal_surname'], parameters['personal_email'], 
        parameters['personal_telefone'], parameters['address_cep'], parameters['address_logradouro'], parameters['address_bairro'], 
        parameters['address_numero'], parameters['address_localidade'], parameters['address_estado']
      )
      if not isValid:
        message = "Usuário já cadastrado!"
      
      isValid = conn.addNewUrls(parameters['personal_cpf'], parameters['urls'])
      if not isValid:
        message = "Urls já cadastradas!"

      return {"OK": True, "Message": message}

    except:
      return {"Ok":False, "Message": "Não foi possível adicionar novo registro!"}

class GetRegister(Resource):
  def get(self,user_cpf):
    try:
      conn = dbConnection.DB()
      data = conn.getUserInfo(user_cpf)
      return {"OK": True, "userInfo": data}
    
    except:
      return {"OK": False, "Message":"Não foi possível obter as informações do utilizador!"}

class GetAllInfo(Resource):
  def get(self):
    try:
      conn = dbConnection.DB()
      data = conn.GetAllInfo()
      return {"OK": True, "UsersInfo": data}
    except:
      return {"OK": False, "Message": "Não foi possível obter as informações!"}


api.add_resource(AddRegister,'/api/json/addRegister')
api.add_resource(GetRegister,'/api/json/getRegister/<string:user_cpf>')
api.add_resource(GetAllInfo,'/api/json/getAllInfo')

if(__name__ == '__main__'):
  app.run(host='127.0.0.1', port=8000, debug=False)
