import sqlite3

class DB:
    def __init__(self):
        self.conn = sqlite3.connect('file:DB/siteBlindado.db', uri=True) 
        self.cursor = self.conn.cursor()
        self.createTables()

    
    def createTables(self):
        try:
            self.cursor.execute('CREATE TABLE IF NOT EXISTS users(cpf INTEGER PRIMARY KEY NOT NULL, nome NVARCHAR(70) NOT NULL, sobrenome NVARCHAR(70), telefone INTEGER, email NVARCHAR(70), cep INTEGER, logradouro NVARCHAR(100), bairro NVARCHAR(70), numero INTEGER, cidade NVARCHAR(70), estado NVARCHAR(10))')
            self.cursor.execute('CREATE TABLE IF NOT EXISTS urls(user_cpf INTEGER NOT NULL, url NVARCHAR(100))')
            self.conn.commit()
        except Exception as e:
            return e.args


    def checkExistingUser(self, cpf):
        try:
            self.cursor.execute("SELECT * FROM users WHERE cpf = ?", (cpf,))
            return len(self.cursor.fetchall()) > 0
        except:
            return False

    def checkExistingUrl(self, cpf, url):
        try:
            self.cursor.execute("SELECT * FROM urls WHERE user_cpf = ? AND url = ?", (cpf, url,))
            return len(self.cursor.fetchone()) > 0
        except:
            return False

    def addNewUser(self, cpf, nome, sobrenome=None, email=None, telefone=None, cep=None, logradouro=None, bairro=None, numero=None, cidade=None, estado=None):
        try:
            userExists = self.checkExistingUser(cpf)
            if userExists:
                return False
            self.cursor.execute('''
                INSERT INTO users 
                (cpf, nome, sobrenome, email, telefone, cep, logradouro, bairro, numero, cidade, estado) 
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)''',
                (cpf, nome, sobrenome, email, telefone, cep, logradouro, bairro, numero, cidade, estado)
            )
            self.conn.commit()
            return True
        
        except:
            self.conn.rollback()
            return False

    def addNewUrls(self, cpf, urls):
        try:
            OK = False
            for url in urls:
                urlExists = self.checkExistingUrl(cpf, url)
                if urlExists:
                    continue
                else:
                    self.cursor.execute("INSERT INTO urls (user_cpf, url) VALUES (?, ?)",(cpf, url,))
                    self.conn.commit()
                    OK = True
                
            return OK
        except:
            self.conn.rollback()
            return False

    def getUserInfo(self, cpf):
        try:
            self.cursor.execute('''
                SELECT users.*, GROUP_CONCAT(urls.url) as urls
                FROM users 
                INNER JOIN urls ON urls.user_cpf = users.cpf
                WHERE users.cpf = ?
                GROUP BY users.cpf''',
                (cpf,)
            )
            data = self.cursor.fetchall()
            return data
        except:
            return False
    
    def GetAllInfo(self):
        try:
            self.cursor.execute('''
                SELECT users.*, GROUP_CONCAT(urls.url) as urls
                FROM users
                INNER JOIN urls ON urls.user_cpf = users.cpf
                GROUP BY users.cpf
            ''')
            data = self.cursor.fetchall()
            return data
        except:
            return False