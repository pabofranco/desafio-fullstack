import React, { Component } from "react";
import InputEmail from "./inputEmail";
import InputName from "./inputName";
import UrlHolder from "./urlHolder";
import InputAddress from "./inputAddress";
import InputCPF from "./inputCPF";
import InputTelefone from "./inputTelefone";
import API from "../API/api.js";

class MainContainer extends Component {
  state = {
    isValid: true,
    urls: "",
    personal_email: "",
    personal_telefone: "",
    personal_cpf: "",
    personal_name: "",
    personal_surname: "",
    address_bairro: "",
    address_cep: "",
    address_estado: "",
    address_localidade: "",
    address_logradouro: "",
    address_numero: ""
  };

  onHandleSubmit = () => {
    const isValid = this.state.isValid;

    if (
      this.state.personal_cpf === "" ||
      this.state.personal_name === "" ||
      this.state.urls === ""
    ) {
      alert("Há campos em branco!");
      return false;
    }

    if (!isValid) {
      alert("Há erros no formulário!");
      return false;
    }

    API.post("addRegister", this.gatherData(), {
      headers: {
        "Content-Type": "application/x-www-form-urlencoded"
      }
    }).then(res => {
      alert("Dados enviados com sucesso!");
      this.clearFields();
    });
  };

  clearFields = () => {
    document.getElementsByClassName("url-list")[0].innerHTML = "";
    const inputs = document.getElementsByTagName("input");
    for (let i = 0; i < inputs.length; i++) inputs[i].value = "";
  };

  handleOnChange = (name, value) => {
    this.setState({ [name]: value });
  };

  onHandleValidate = status => {
    this.setState({ isValid: status });
  };

  gatherData = () => {
    return {
      urls: this.state.urls,
      personal_email: this.state.personal_email,
      personal_telefone: this.state.personal_telefone,
      personal_cpf: this.state.personal_cpf,
      personal_name: this.state.personal_name,
      personal_surname: this.state.personal_surname,
      address_bairro: this.state.address_bairro,
      address_cep: this.state.address_cep,
      address_estado: this.state.address_estado,
      address_localidade: this.state.address_localidade,
      address_logradouro: this.state.address_logradouro,
      address_numero: this.state.address_numero
    };
  };

  render() {
    return (
      <React.Fragment>
        <span className="righty">
          <small>
            Campos Obrigatórios <span className="error-label">*</span>
          </small>
        </span>
        <h2>Dados Pessoais</h2>
        <hr />
        <div className="row">
          <InputName
            name="Nome"
            length="30"
            surname="Sobrenome"
            onHandleValidate={this.onHandleValidate}
            handleOnChange={this.handleOnChange}
            isRequired={true}
          />
        </div>
        <div className="row">
          <InputEmail
            name="Email"
            length="25"
            name_confirmation="Confirmar E-mail"
            onHandleValidate={this.onHandleValidate}
            handleOnChange={this.handleOnChange}
            isRequired={false}
          />
        </div>
        <div className="row">
          <InputCPF
            name="CPF"
            onHandleValidate={this.onHandleValidate}
            handleOnChange={this.handleOnChange}
            isRequired={true}
          />
          <InputTelefone
            name="Telefone"
            handleOnChange={this.handleOnChange}
            isRequired={false}
          />
        </div>
        <br />
        <h2>Endereço</h2>
        <hr />
        <InputAddress handleOnChange={this.handleOnChange} isRequired={false} />
        <br />
        <h2>
          Urls <span className="error-label">*</span>
        </h2>
        <hr />
        <UrlHolder
          onHandleValidate={this.onHandleValidate}
          handleOnChange={this.handleOnChange}
          isRequired={true}
        />
        <br />
        <div className="row">
          <div className="col-sm-12 col-md-12 col-lg-12">
            <button
              className="btn btn-lg btn-primary send-btn"
              onClick={this.onHandleSubmit}
            >
              Enviar
            </button>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default MainContainer;
