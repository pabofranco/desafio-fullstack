import React, { Component } from "react";

class Url extends Component {
  state = {
    info: this.props.element
  };

  render() {
    return (
      <React.Fragment>
        <br />
        <button
          key={"btn-" + this.state.info.id}
          className="btn btn-sm btn-danger"
          onClick={() => this.props.onDelete(this.state.info.id)}
        >
          x
        </button>
        {/* <label className="m-2">#{this.state.info.id} - </label> */}
        <span className="url m-2" key={this.state.info.id}>
          {this.state.info.address}
        </span>
      </React.Fragment>
    );
  }
}

export default Url;
