import React, { Component } from "react";

class TopBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      description: this.props.description
    };
  }
  render() {
    return (
      <div className="jumbotron jumbotron-fluid fixed-top">
        <div className="container">
          <h2 className="topTitle">{this.state.description}</h2>
        </div>
      </div>
    );
  }
}

export default TopBar;
