import React, { Component } from "react";
import utils from "../scripts/utils";

class InputCPF extends Component {
  state = {
    name: this.props.name,
    personal_cpf: "",
    errorMessage: "",
    isRequired: this.props.isRequired
  };

  handleOnChange = () => event => {
    const value = event.target.value;
    this.setState({ personal_cpf: value });
    this.props.handleOnChange("personal_cpf", value);
  };

  setErrorMessage = status => {
    let errorMessage = status
      ? ""
      : this.state.personal_cpf === ""
      ? "Campo obrigatório!"
      : "Campo inválido!";

    this.setState({ errorMessage });
  };

  handleValidate = cpf => {
    const status = utils.validate(/^\d{11}$/, cpf);
    this.setErrorMessage(status);
    this.props.onHandleValidate(status);
  };

  render() {
    return (
      <div className="col-sm-6 col-md-6 col-lg-6">
        <div className="form-group">
          <label htmlFor="personal_cpf" className="m-2">
            {this.state.name}{" "}
            {this.state.isRequired ? (
              <span className="error-label">*</span>
            ) : null}
          </label>
          <input
            className="form-control"
            id="personal_cpf"
            type="text"
            size="10"
            onChange={this.handleOnChange()}
            onBlur={() => {
              this.handleValidate(this.state.personal_cpf);
            }}
          />
          <small className="error-label">{this.state.errorMessage}</small>
        </div>
      </div>
    );
  }
}

export default InputCPF;
