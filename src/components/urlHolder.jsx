import React, { Component } from "react";
import Url from "./url";
import utils from "../scripts/utils";

class UrlHolder extends Component {
  state = {
    urls: [],
    newUrlValue: "",
    errorMessage: "",
    isRequired: this.props.isRequired
  };

  handleUrlChange = () => event => {
    this.setState({ newUrlValue: event.target.value });
  };

  handleUrlError = (isValid, exists) => {
    let errorMessage = this.state.errorMessage;
    if (exists) errorMessage = "O endereço já existe na lista!";
    if (!isValid)
      errorMessage = utils.handleErrorMessage(isValid, this.state.newUrlValue);

    this.setState({ errorMessage });
  };

  validateUrl = url => {
    const isValid = utils.validate(
      /^(http:\/\/www.|https:\/\/www.|http:\/\/|https:\/\/)?[a-z0-9]+([-.]{1}[a-z0-9]+)*.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/,
      url
    );
    const exists = this.urlExists(url);

    if (!isValid || exists) this.handleUrlError(isValid, exists);

    return isValid && !exists;
  };

  urlExists = url => {
    return this.state.urls.find(el => el.address === url) || false;
  };

  handleDeleteUrl = url_id => {
    const urls = this.state.urls.filter(element => element.id !== url_id);
    this.setState({ urls });
  };

  getUrlsToSend = urls => {
    let toSend = [];
    for (let i = 0; i < urls.length; i++) {
      toSend.push(urls[i].address);
    }
    return toSend;
  };

  handleAddUrl = url => {
    const urlToAdd = utils.getProperUrl(url); // verifica 'http(s)://'
    const isValid = this.validateUrl(urlToAdd);

    if (isValid) {
      const newId = this.state.urls.length + 1;
      let newUrls = this.state.urls;

      newUrls.push({ id: newId, address: urlToAdd });
      this.props.handleOnChange("urls", this.getUrlsToSend(newUrls));

      document.getElementById("input_add_url").value = ""; // limpa input
      this.setState({ newUrlValue: "" });
    }
  };

  render() {
    return (
      <React.Fragment>
        <div className="form-group">
          <input
            type="text"
            id="input_add_url"
            size="80"
            defaultValue={this.state.newUrlValue}
            onChange={this.handleUrlChange()}
          />
          <button
            className="btn btn-primary bottom"
            onClick={() => this.handleAddUrl(this.state.newUrlValue)}
          >
            Adicionar URL
          </button>
          <br />
          <small className="error-label">{this.state.errorMessage}</small>
        </div>

        <div className="form-control url-container">
          <ul className="url-list">
            {this.state.urls.map(url => (
              <li key={url.id}>
                <Url
                  key={url.id}
                  element={url}
                  onDelete={() => this.handleDeleteUrl(url.id)}
                />
              </li>
            ))}
          </ul>
        </div>
      </React.Fragment>
    );
  }
}

export default UrlHolder;
