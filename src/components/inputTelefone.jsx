import React, { Component } from "react";

class InputTelefone extends Component {
  state = {
    name: this.props.name,
    personal_telefone: "",
    telefoneError: "",
    isRequired: this.props.isRequired
  };

  handleOnChange = () => event => {
    const value = event.target.value;
    this.setState({ personal_telefone: value });
    this.props.handleOnChange("personal_telefone", value);
  };

  render() {
    return (
      <div className="col-sm-6 col-md-6 col-lg-6">
        <div className="form-group">
          <label htmlFor="personal_telefone" className="m-2">
            {this.state.name}{" "}
            {this.state.isRequired ? (
              <span className="error-label">*</span>
            ) : null}
          </label>
          <input
            className="form-control"
            id="personal_telefone"
            type="text"
            size="10"
            defaultValue={this.state.personal_telefone}
            onChange={this.handleOnChange()}
          />
          <small className="error-label">{this.state.telefoneError}</small>
        </div>
      </div>
    );
  }
}

export default InputTelefone;
