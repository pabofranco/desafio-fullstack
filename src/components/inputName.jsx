import React, { Component } from "react";
import utils from "../scripts/utils";

class InputName extends Component {
  state = {
    name: this.props.name,
    length: this.props.length,
    surname: this.props.surname,
    personal_name: "",
    personal_surname: "",
    errorMessage: "",
    isRequired: this.props.isRequired
  };

  hasSurname = () => {
    if (this.state.surname) {
      return (
        <div className="col-sm-6 col-md-6 col-lg-6">
          <div className="form-group">
            <label htmlFor="personal_surname" className="m-2">
              {this.state.surname}
            </label>
            <input
              className="form-control"
              id="personal_surname"
              type="text"
              size={this.state.length}
              onChange={() => this.handleOnChange("personal_surname")}
            />
          </div>
        </div>
      );
    }
    return null;
  };

  handleOnChange = element => {
    const value = document.getElementById(element).value;
    this.setState({ [element]: value });
    this.props.handleOnChange(element, value);
  };

  handleValidate = status => {
    this.props.onHandleValidate(status);
  };

  validate = name => {
    const isValid = utils.validate(
      /^[A-Z][a-zA-Z][^#&<>"~;$^%{}?]{1,20}$/,
      name
    );
    const errorMessage = utils.handleErrorMessage(
      isValid,
      this.state.personal_name
    );
    this.setState({ errorMessage });
    this.handleValidate(isValid);
  };

  render() {
    return (
      <React.Fragment>
        <div className={utils.getColSize(this.state.surname)}>
          <div className="form-group">
            <label htmlFor="personal_name" className="m-2">
              {this.state.name}{" "}
              {this.state.isRequired ? (
                <span className="error-label">*</span>
              ) : null}
            </label>
            <input
              className="form-control"
              id="personal_name"
              type="text"
              size={this.state.length}
              onChange={() => this.handleOnChange("personal_name")}
              onBlur={() => {
                this.validate(this.state.personal_name);
              }}
            />
            <small className="error-label">{this.state.errorMessage}</small>
          </div>
        </div>
        {this.hasSurname()}
      </React.Fragment>
    );
  }
}

export default InputName;
