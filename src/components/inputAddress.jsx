import React, { Component } from "react";
import Axios from "axios";
import utils from "../scripts/utils";

class InputAddress extends Component {
  state = {
    address_bairro: "",
    address_cep: "",
    address_estado: "",
    address_localidade: "",
    address_logradouro: "",
    address_numero: "",
    bairroError: "",
    cepError: "",
    estadoError: "",
    localidadeError: "",
    logradouroError: "",
    numeroErrror: "",
    isRequired: this.props.isRequired
  };

  handleOnChange = element => {
    const value = document.getElementById(element).value;
    this.setState({ [element]: value });
    this.props.handleOnChange(element, value);
  };

  getAddressFromCep = () => {
    const cep = this.state.address_cep;
    const isValid = utils.validate(/^[0-9]{8}$/, cep);
    if (isValid) {
      this.setState({ cepError: "" });
      const url = "https://viacep.com.br/ws/" + cep + "/json/unicode";

      Axios.get(url).then(res => {
        const data = res.data;

        if (data.erro) {
          this.setState({ cepError: "CEP não encontrado" });
          return false;
        }

        this.setState({
          address_logradouro: data.logradouro,
          address_bairro: data.bairro,
          address_localidade: data.localidade,
          address_estado: data.uf
        });

        this.props.handleOnChange("address_logradouro", data.logradouro);
        this.props.handleOnChange("address_bairro", data.bairro);
        this.props.handleOnChange("address_localidade", data.localidade);
        this.props.handleOnChange("address_estado", data.uf);
      });
    } else this.setState({ cepError: "CEP inválido" });
  };
  render() {
    return (
      <React.Fragment>
        <div className="row">
          <div className="col-sm-4 col-md-4 col-lg-4">
            <div className="form-group">
              <label htmlFor="address_cep" className="m-2">
                CEP{" "}
                {this.state.isRequired ? (
                  <span className="error-label">*</span>
                ) : null}
              </label>
              <input
                className="form-control"
                type="text"
                size="5"
                id="address_cep"
                defaultValue={this.state.address_cep}
                onChange={() => this.handleOnChange("address_cep")}
              />
              <small className="error-label">{this.state.cepError}</small>
            </div>
          </div>
          <div className="col-sm-2 col-md-2 col-lg-2">
            <button
              className="btn btn-primary m-2 btn-search-cep"
              onClick={() => this.getAddressFromCep()}
            >
              Buscar
            </button>
          </div>
        </div>
        <div className="row">
          <div className="col-sm-4 col-md-4 col-lg-4">
            <div className="form-group">
              <label htmlFor="address_logradouro" className="m-2">
                Logradouro
              </label>
              <input
                className="form-control"
                type="text"
                id="address_logradouro"
                size="50"
                defaultValue={this.state.address_logradouro}
                onChange={() => this.handleOnChange("address_logradouro")}
                disabled
              />
              <small className="error-label">
                {this.state.logradouroError}
              </small>
            </div>
          </div>

          <div className="col-sm-4 col-md-4 col-lg-4">
            <div className="form-group">
              <label htmlFor="address_bairro" className="m-2">
                Bairro
              </label>
              <input
                className="form-control"
                type="text"
                id="address_bairro"
                size="30"
                defaultValue={this.state.address_bairro}
                onChange={() => this.handleOnChange("address_bairro")}
                disabled
              />
              <small className="error-label">{this.state.bairroError}</small>
            </div>
          </div>
          <div className="col-sm-2 col-md-2 col-lg-2">
            <div className="form-group">
              <label htmlFor="address_numero" className="m-2">
                Número{" "}
                {this.state.isRequired ? (
                  <span className="error-label">*</span>
                ) : null}
              </label>
              <input
                className="form-control"
                type="text"
                id="address_numero"
                size="5"
                onChange={() => this.handleOnChange("address_numero")}
                defaultValue={this.state.address_numero}
              />
              <small className="error-label">{this.state.numeroErrror}</small>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-sm-4 col-md-4 col-lg-4">
            <div className="form-group">
              <label htmlFor="address_localidade" className="m-2">
                Cidade
              </label>
              <input
                className="form-control"
                type="text"
                id="address_localidade"
                size="30"
                defaultValue={this.state.address_localidade}
                onChange={() => this.handleOnChange("address_localidade")}
                disabled
              />
              <small className="error-label">
                {this.state.localidadeError}
              </small>
            </div>
          </div>
          <div className="col-sm-4 col-md-4 col-lg-4">
            <div className="form-group">
              <label htmlFor="address_estado" className="m-2">
                Estado
              </label>
              <input
                className="form-control"
                type="text"
                id="address_estado"
                size="2"
                defaultValue={this.state.address_estado}
                onChange={() => this.handleOnChange("address_estado")}
                disabled
              />
              <small className="error-label">{this.state.estadoError}</small>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default InputAddress;
