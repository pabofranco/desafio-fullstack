import React, { Component } from "react";
import utils from "../scripts/utils";

class InputEmail extends Component {
  state = {
    name: this.props.name,
    name_confirmation: this.props.name_confirmation,
    length: this.props.length,
    personal_email: "",
    personal_email_confirmation: "",
    errorMessage: "",
    errorMessageConfirmation: "",
    isValid: true,
    isRequired: this.props.isRequired
  };

  handleOnChange = element => {
    let value = document.getElementById(element).value;
    this.setState({ [element]: value });
    this.props.handleOnChange([element], value);
  };

  handleValidate = status => {
    this.props.onHandleValidate(status);
  };

  validate = () => {
    const isEmailValid = utils.validate(
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
      this.state.personal_email
    );
    const isConfirmationValid = utils.validate(
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
      this.state.personal_email_confirmation
    );
    const message = utils.handleErrorMessage(
      isEmailValid,
      this.state.personal_email
    );
    let confirm_message = utils.handleErrorMessage(
      isConfirmationValid,
      this.state.personal_email_confirmation
    );

    if (
      isEmailValid &&
      isConfirmationValid &&
      this.state.personal_email_confirmation !== this.state.personal_email
    )
      confirm_message = "Os e-mails são diferentes!";

    if (
      !isEmailValid ||
      !isConfirmationValid ||
      !this.state.personal_email_confirmation === this.state.personal_email
    )
      this.handleValidate(false);
    else this.handleValidate(true);

    this.setState({
      errorMessage: message,
      errorMessageConfirmation: confirm_message
    });
  };

  hasConfirmation = () => {
    if (this.state.name_confirmation)
      return (
        <div className="col-sm-6 col-md-6 col-lg-6">
          <div className="form-group">
            <label htmlFor="personal_email_confirmation" className="m-2">
              {this.state.name_confirmation}{" "}
              {this.state.isRequired ? (
                <span className="error-label">*</span>
              ) : null}
            </label>
            <input
              className="form-control"
              id="personal_email_confirmation"
              type="email"
              size={this.state.length}
              onChange={() =>
                this.handleOnChange("personal_email_confirmation")
              }
              onBlur={this.validate}
            />
            <small className="error-label">
              {this.state.errorMessageConfirmation}
            </small>
          </div>
        </div>
      );

    return null;
  };

  render() {
    return (
      <React.Fragment>
        <div className={utils.getColSize(this.state.name_confirmation)}>
          <div className="form-group">
            <label htmlFor="personal_email" className="m-2">
              {this.state.name}{" "}
              {this.state.isRequired ? (
                <span className="error-label">*</span>
              ) : null}
            </label>
            <input
              className="form-control"
              id="personal_email"
              type="email"
              size={this.state.length}
              onChange={() => this.handleOnChange("personal_email")}
              onBlur={this.validate}
            />
            <small className="error-label">{this.state.errorMessage}</small>
          </div>
        </div>
        {this.hasConfirmation()}
      </React.Fragment>
    );
  }
}

export default InputEmail;
