// handle validations
let utils = {};

utils.validate = (expression, value) => {
  return expression.test(value);
};
utils.getColSize = field => {
  if (field) return "col-sm-6 col-md-6 col-lg-6";
  return "col-sm-12 col-md-12 col-lg-12";
};
utils.handleErrorMessage = (status, field) => {
  return status ? "" : field === "" ? "Campo obrigatório!" : "Campo inválido!";
};
utils.getProperUrl = url => {
  if (url.indexOf("http://") < 0 && url.indexOf("https://") < 0)
    return "http://" + url;

  return url;
};
module.exports = utils;
