import React from "react";
import ReactDOM from "react-dom";
import "bootstrap/dist/css/bootstrap.css";
import "./css/app.css";
import TopBar from "./components/topBar";
import MainContainer from "./components/mainContainer";

// ReactDOM.render(<Counters />, document.getElementById("root"));
ReactDOM.render(
  <TopBar description="Site Blindado" />,
  document.getElementById("topBar")
);
ReactDOM.render(<MainContainer />, document.getElementById("main"));
