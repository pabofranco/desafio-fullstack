const assert = require("chai").assert;
const utils = require("../../../src/scripts/utils");

// expressions to test
const cpf_expression = /^\d{11}$/;
const cep_expression = /^[0-9]{8}$/;
const name_expression = /^[A-Z][a-zA-Z][^#&<>"~;$^%{}?]{1,20}$/;
const email_expression = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const url_expression = /^(http:\/\/www.|https:\/\/www.|http:\/\/|https:\/\/)?[a-z0-9]+([-.]{1}[a-z0-9]+)*.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/;
const validUrlHttp = "http://www.google.com";
const validUrlHttps = "https://www.google.com";
const validUrl = "www.google.com";
const faultyUrl = "www.part.2";
const properFaultyUrl = "http://www.part.2";

describe("Utils test", () => {
  describe("Validation method", () => {
    describe("CPF validation", () => {
      it("Should return a boolean value", () => {
        assert.typeOf(utils.validate(cpf_expression, 35707088881), "boolean");
      });
      it("Should return true for 11 digits numbers", () => {
        assert.equal(utils.validate(cpf_expression, 35707088881), true);
      });
      it("Should return false for more than 11 digits number", () => {
        assert.equal(utils.validate(cpf_expression, 357070888811), false);
      });
      it("Should return false for less than 11 digits number", () => {
        assert.equal(utils.validate(cpf_expression, 3570708888), false);
      });
      it("Should return false for empty value", () => {
        assert.equal(utils.validate(cpf_expression, ""), false);
      });
    });
    describe("CEP validation", () => {
      it("Should return a boolean value", () => {
        assert.typeOf(utils.validate(cep_expression, "05591090"), "boolean");
      });
      it("Should return true for 8 digits numbers", () => {
        assert.equal(utils.validate(cep_expression, "05591090"), true);
      });
      it("Should return false if there is anything but digits", () => {
        assert.equal(utils.validate(cep_expression, "05591-090"), false);
      });
      it("Should return false for more than 8 digits number", () => {
        assert.equal(utils.validate(cep_expression, "055910901"), false);
      });
      it("Should return false for less than 8 digits number", () => {
        assert.equal(utils.validate(cep_expression, "055910"), false);
      });
      it("Should return false for empty value", () => {
        assert.equal(utils.validate(cep_expression, ""), false);
      });
    });
    describe("Name validation", () => {
      it("Should return a boolean value", () => {
        assert.typeOf(
          utils.validate(name_expression, "Pedro Augusto"),
          "boolean"
        );
      });
      it("Should return false if first letter is lower case", () => {
        assert.equal(utils.validate(name_expression, "pedro augusto"), false);
      });
      it("Should return false if length is lesser than 3", () => {
        assert.equal(utils.validate(name_expression, "El"), false);
      });
      it("Should return false for empty value", () => {
        assert.equal(utils.validate(name_expression, ""), false);
      });
      it("Should return false if it starts with anything but upper case letters (space)", () => {
        assert.equal(utils.validate(name_expression, " Pedro Augusto"), false);
      });
      it("Should return false if it starts with anything but upper case letters (underscore)", () => {
        assert.equal(utils.validate(name_expression, "_Pedro Augusto"), false);
      });
    });
    describe("E-mail validation", () => {
      it("Should return a boolean value", () => {
        assert.typeOf(
          utils.validate(email_expression, "pabofranco@gmail.com"),
          "boolean"
        );
      });
      it("Should return true for a valid e-mail address", () => {
        assert.equal(
          utils.validate(email_expression, "pabofranco@gmail.com"),
          true
        );
      });
      it("Should return true if it is lower case, upper case or camel case", () => {
        assert.equal(
          utils.validate(email_expression, "PaboFranco@gmail.com"),
          true
        );
      });
      it("Should return false if it contains a space", () => {
        assert.equal(
          utils.validate(email_expression, " pabofranco@gmail.com"),
          false
        );
      });
      it("Should return false for empty value", () => {
        assert.equal(utils.validate(email_expression, ""), false);
      });
    });
    describe("URL validation", () => {
      it("Should return a boolean value", () => {
        assert.typeOf(utils.validate(url_expression, validUrlHttps), "boolean");
      });
      it("Should return true for a valid url with a starting 'http://'", () => {
        assert.equal(utils.validate(url_expression, validUrlHttp), true);
      });
      it("Should return true for a valid url with a starting 'https://'", () => {
        assert.equal(utils.validate(url_expression, validUrlHttps), true);
      });
      it("Should return true for a valid url without a starting 'http://' or 'https://", () => {
        assert.equal(utils.validate(url_expression, validUrl), true);
      });
      it("Should return false for an invalid url", () => {
        assert.equal(utils.validate(url_expression, faultyUrl), false);
      });
      it("Should return false for an invalid url even if it is has been proper formatted", () => {
        assert.equal(utils.validate(url_expression, properFaultyUrl), false);
      });
    });
  });
  describe("Get column size method", () => {
    it("Should return a string", () => {
      assert.typeOf(utils.getColSize("name"), "string");
    });
    it("Should return 'col-sm-6 col-md-6 col-lg-6' if parameter is not empty", () => {
      assert.equal(utils.getColSize("not empty"), "col-sm-6 col-md-6 col-lg-6");
    });
    it("Should return 'col-sm-12 col-md-12 col-lg-12' if parameter is empty", () => {
      assert.equal(utils.getColSize(""), "col-sm-12 col-md-12 col-lg-12");
    });
  });
  describe("Handle error message method", () => {
    it("Should return a string", () => {
      assert.typeOf(utils.handleErrorMessage(false, ""), "string");
    });
    it("Should return an empty string if first argument is true", () => {
      assert.equal(utils.handleErrorMessage(true, "second argument"), "");
    });
    it("Should return 'Campo obrigatório!' if first argument is false AND second argument is empty", () => {
      assert.equal(utils.handleErrorMessage(false, ""), "Campo obrigatório!");
    });
    it("Should return 'Campo inválido!' if first argument is false AND second argument is not empty", () => {
      assert.equal(
        utils.handleErrorMessage(false, "not empty"),
        "Campo inválido!"
      );
    });
  });
  describe("Get proper URL method", () => {
    it("Should return a string", () => {
      assert.typeOf(utils.getProperUrl("http://www.google.com"), "string");
    });
    it("Should return the same string if it contains 'http://'", () => {
      assert.equal(utils.getProperUrl(validUrlHttp), validUrlHttp);
    });
    it("Should return the same string if it contains 'https://'", () => {
      assert.equal(utils.getProperUrl(validUrlHttps), validUrlHttps);
    });
    it("Should return 'http:// + url' if it does not contain neither 'http://' nor 'https://'", () => {
      assert.equal(utils.getProperUrl(validUrl), validUrlHttp);
    });
    it("Should return 'http:// + url' even if it not a valid url", () => {
      assert.equal(utils.getProperUrl(faultyUrl), properFaultyUrl);
    });
  });
});
